FROM nvidiajetson/l4t-ros2-foxy-pytorch:r32.5

ENV ROBOCOON_WS=/robocoon_ws

RUN apt-get install -y software-properties-common  \
    && add-apt-repository ppa:ubuntu-toolchain-r/test  \
    && apt-get update -y \
    && apt-get install gcc-9 g++-9 -y

ENV CC=/usr/bin/gcc-9
ENV CXX=/usr/bin/g++-9

WORKDIR ${ROS_ROOT}
COPY ros2_controllers ros2_controllers

RUN rosinstall_generator --tar --deps --deps-only --from-path ros2_controllers/ --exclude-path src/ > ros2_controllers_deps.rosinstall  \
    && vcs import src < ros2_controllers_deps.rosinstall && mv ros2_controllers src
RUN rosinstall_generator --tar --deps xacro --exclude-path src/ > robocoon_deps.rosinstall \
    && vcs import src < robocoon_deps.rosinstall
RUN colcon build --packages-skip-build-finished

WORKDIR ${ROBOCOON_WS}
COPY src src

RUN git clone https://gitlab.com/r1540/robocoon_common.git src/robocoon_common

RUN . ${ROS_ROOT}/install/setup.sh  \
    && colcon build

COPY robocoon_entrypoint.sh /robocoon_entrypoint.sh
RUN chmod +x /robocoon_entrypoint.sh
ENTRYPOINT ["/robocoon_entrypoint.sh"]
CMD ["bash"]