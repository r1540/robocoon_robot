#!/bin/bash
set -e

source ${ROBOCOON_WS}/install/setup.bash
exec "$@"