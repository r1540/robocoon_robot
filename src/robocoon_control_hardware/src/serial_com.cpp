#include "serial_com.h"

namespace robocoon_control_hardware {

SerialCom::SerialCom(const ComConfig &config) : m_serial(), m_timeout() {
  m_serial.setTimeout(m_timeout);
  m_serial.setPort(config.port);
  m_serial.setBaudrate(config.baudrate);
}
size_t SerialCom::write(std::string string) {
  return m_serial.write(string);
}
std::string SerialCom::readline(uint32_t timeout) {
  m_timeout = serial::Timeout::simpleTimeout(timeout);
  m_serial.setTimeout(m_timeout);
  return m_serial.readline();
}
std::tuple<bool, std::string> SerialCom::open() {
  bool ret = false;
  std::string errExplanation;

  try {
    m_serial.open();
    ret = true;
  } catch (const std::invalid_argument &e) {
    errExplanation = "Invalid argument: " + std::string(e.what());
  } catch (const serial::SerialException &e) {
    errExplanation = "Serial exception: " + std::string(e.what());
  } catch (const serial::IOException &e) {
    errExplanation = "IO exception: " + std::string(e.what());
  }
  return {ret, errExplanation};
}
void SerialCom::close() {
  m_serial.close();
}
}