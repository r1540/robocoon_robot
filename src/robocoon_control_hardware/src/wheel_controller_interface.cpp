#include "robocoon_control/wheel_controller_interface.h"
#include "pluginlib/class_list_macros.hpp"
#include "rclcpp/logging.hpp"
#include "rclcpp/clock.hpp"
#include <regex>
#include <numeric>

std::vector<std::string> split(const std::string &str, const std::string &regex_str) {
  std::regex r(regex_str);
  return {std::sregex_token_iterator(str.begin(), str.end(), r, -1), std::sregex_token_iterator()};
}

namespace robocoon_control_hardware {

hardware_interface::return_type WheelControllerInterface::configure(const hardware_interface::HardwareInfo &system_info) {
  m_name = system_info.name;

  if (system_info.type != "system") {
    RCLCPP_ERROR(m_logger, "WheelControllerInterface supports only system component");
    return hardware_interface::return_type::ERROR;
  }
  if (!system_info.gpios.empty()) {
    RCLCPP_WARN(m_logger, "GPIOs are not supported");
  }
  if (!system_info.sensors.empty()) {
    RCLCPP_WARN(m_logger, "sensors are not supported");
  }
  if (!system_info.transmissions.empty()) {
    RCLCPP_WARN(m_logger, "transmissions are not supported");
  }

  for (const auto &j : system_info.joints) {
    auto index = std::stoul(j.parameters.at("index"));
    if (index >= NUM_OF_WHEELS) {
      RCLCPP_ERROR_STREAM(m_logger, "Invalid index param [" << index << "] for joint " << j.name);
      return hardware_interface::return_type::ERROR;
    }
    m_stateInterfaces.emplace_back(j.name, "position", &m_zero);
    m_commandInterfaces.emplace_back(j.name, j.command_interfaces[0].name, &m_commands[index]);
    m_stateInterfaces.emplace_back(j.name, j.state_interfaces[0].name, &m_state[index]);
    auto multiplier = std::stod(j.parameters.at("multiplier"));
    m_multipliers[index] = multiplier;
  }

  for (auto &speed : m_speeds) {
    speed = std::deque<double>(10, 0.);
  }

  m_serialPort = Com::getCom({115200u, "/dev/ttyACM0"});
  m_status = hardware_interface::status::CONFIGURED;
  return hardware_interface::return_type::OK;
}
std::vector<hardware_interface::StateInterface> WheelControllerInterface::export_state_interfaces() {
  return m_stateInterfaces;
}
std::vector<hardware_interface::CommandInterface> WheelControllerInterface::export_command_interfaces() {
  return std::move(m_commandInterfaces);
}
hardware_interface::return_type WheelControllerInterface::start() {
  const auto [status, log] = m_serialPort->open();
  if (status) {
    m_status = hardware_interface::status::STARTED;
  }
  RCLCPP_ERROR_STREAM_EXPRESSION(m_logger, !status, "Could not open serial port. Reason: " << log);
  return status ? hardware_interface::return_type::OK : hardware_interface::return_type::ERROR;
}
hardware_interface::return_type WheelControllerInterface::stop() {
  m_serialPort->close();
  m_status = hardware_interface::status::STOPPED;
  return hardware_interface::return_type::OK;
}
std::string WheelControllerInterface::get_name() const {
  return m_name;
}
hardware_interface::status WheelControllerInterface::get_status() const {
  return m_status;
}
hardware_interface::return_type WheelControllerInterface::read() {
  std::string l = m_serialPort->readline(500);

  if (!l.empty()) {
    auto tokens = split(l, ",");

    if (tokens.size() != NUM_OF_WHEELS) {
      RCLCPP_WARN_STREAM(m_logger, "WRONG DATA RECEIVED: " << l);
    } else {
      try {
        std::array<double, NUM_OF_WHEELS> tokensTransformed{};
        std::transform(tokens.begin(),
                       tokens.end(),
                       tokensTransformed.begin(),
                       [](const auto &t) { return std::stod(t); });
        processWheelSpeeds(tokensTransformed);
      } catch (const std::invalid_argument &) {
        RCLCPP_WARN_STREAM(m_logger, "Wrongly formatted line: " << l);
      }
    }
  } else {
    RCLCPP_DEBUG(m_logger, "Empty string received");
    return hardware_interface::return_type::ERROR;
  }
  return hardware_interface::return_type::OK;
}
hardware_interface::return_type WheelControllerInterface::write() {
  std::stringstream ss;
  auto sep = "";
  for (size_t i = 0; i< NUM_OF_WHEELS; ++i)
  {
    ss << sep << static_cast<int>(m_commands[i] * m_multipliers[i]);
    sep = ",";
  }
  ss << '\n';
  RCLCPP_DEBUG(m_logger, "%s", ss.str().c_str());
  const auto n = m_serialPort->write(ss.str());
  RCLCPP_ERROR_EXPRESSION(m_logger, n == 0, "Failed to send message");
  return n > 0 ? hardware_interface::return_type::OK : hardware_interface::return_type::ERROR;
}
void WheelControllerInterface::processWheelSpeeds(std::array<double, NUM_OF_WHEELS> speeds) {
  auto mean = [](const auto &q) -> double {
    return std::accumulate(q.begin(), q.end(), 0.) / static_cast<double>(q.size());
  };
  auto process = [mean](auto &accQue, double val, double &out) {
    accQue.pop_front();
    accQue.push_back(val);
    out = mean(accQue);
  };

  for (size_t i = 0; i < NUM_OF_WHEELS; ++i) {
    process(m_speeds[i], speeds[i] / m_multipliers[i], m_state[i]);
  }
}

}

PLUGINLIB_EXPORT_CLASS(robocoon_control_hardware::WheelControllerInterface, hardware_interface::SystemInterface)