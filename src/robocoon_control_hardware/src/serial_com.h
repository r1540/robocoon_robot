#ifndef ROBOCOON_CONTROL_HARDWARE_SERIAL_COM_H_
#define ROBOCOON_CONTROL_HARDWARE_SERIAL_COM_H_

#include "robocoon_control/com.h"
#include "serial/serial.h"

namespace robocoon_control_hardware {
class SerialCom : public Com {
 public:
  explicit SerialCom(const ComConfig& config);

  size_t write(std::string string) override;
  std::string readline(uint32_t timeout) override;
  std::tuple<bool, std::string> open() override;
  void close() override;
 private:
  serial::Serial m_serial;
  serial::Timeout m_timeout;
};

}

#endif //ROBOCOON_CONTROL_HARDWARE_SERIAL_COM_H_
