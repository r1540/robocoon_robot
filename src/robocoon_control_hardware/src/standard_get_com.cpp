#include "robocoon_control/com.h"
#include "serial_com.h"

namespace robocoon_control_hardware {
std::unique_ptr<Com> Com::getCom(const ComConfig& comConfig) {
  return std::make_unique<SerialCom>(comConfig);
}
}