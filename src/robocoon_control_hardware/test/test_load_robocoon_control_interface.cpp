#include <gmock/gmock.h>

#include <string>
#include <vector>

#include "hardware_interface/resource_manager.hpp"
#include "ros2_control_test_assets/descriptions.hpp"

class TestGenericSystem : public ::testing::Test {
 protected:
  void SetUp() override {

    hardware_system_2dof_ =
        R"(
  <ros2_control name="GenericSystem2dof" type="system">
    <hardware>
      <plugin>robocoon_control_hardware/WheelControllerInterface</plugin>
    </hardware>
    <joint name="joint1">
      <command_interface name="velocity">
        <param name="min">-1</param>
        <param name="max">1</param>
      </command_interface>
      <state_interface name="velocity"/>
      <param name="wheel">FL</param>
    </joint>
    <joint name="joint2">
      <command_interface name="velocity">
        <param name="min">-1</param>
        <param name="max">1</param>
      </command_interface>
      <state_interface name="velocity"/>
      <param name="wheel">RL</param>
    </joint>
  </ros2_control>
)";

  }

  std::string hardware_system_2dof_;
};

TEST_F(TestGenericSystem, load_generic_system_2dof) {
  auto urdf = ros2_control_test_assets::urdf_head + hardware_system_2dof_ +
      ros2_control_test_assets::urdf_tail;
  hardware_interface::ResourceManager rm1(urdf);
  ASSERT_NO_THROW(hardware_interface::ResourceManager rm(urdf));
}