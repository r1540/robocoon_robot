#include "robocoon_control/wheel_controller_interface.h"
#include <gmock/gmock.h>

using namespace robocoon_control_hardware;
using namespace testing;

class ComMock : public Com {
 public:
  MOCK_METHOD(size_t, write, (std::string), (override));
  MOCK_METHOD(std::string, readline, (uint32_t), (override));
  MOCK_METHOD((std::tuple<bool, std::string>), open, (), (override));
  MOCK_METHOD(void, close, (), (override));

  MOCK_METHOD(void, configCall, (const ComConfig&));

};

std::mutex comMockMutex;
std::unique_lock<std::mutex> comLock(comMockMutex);
ComMock *comMock = nullptr;

std::unique_ptr<Com> Com::getCom(const ComConfig &comConfig) {
  comMock->configCall(comConfig);
  return std::unique_ptr<Com>(comMock);
}

class WheelControllerInterfaceTestBase : public Test {
 protected:
  void SetUp() override {
    comLock.lock();
    comMock = new ComMock();
  }
  void TearDown() override {
    comMock = nullptr;
    comLock.unlock();
  };

};

class WheelControllerInterfaceConfigurationTest : public WheelControllerInterfaceTestBase {

};