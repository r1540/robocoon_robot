#ifndef BUILD_SRC_ROBOCOON_CONTROL_HARDWARE_SRC_COM_H_
#define BUILD_SRC_ROBOCOON_CONTROL_HARDWARE_SRC_COM_H_

#include <memory>

namespace robocoon_control_hardware {
struct ComConfig {
  uint32_t baudrate;
  std::string port;
};
class Com {
 public:
  virtual size_t write(std::string) = 0;
  virtual std::string readline(uint32_t timeout) = 0;
  virtual std::tuple<bool, std::string> open() = 0;
  virtual void close() = 0;
  virtual ~Com() = default;

  static std::unique_ptr<Com> getCom(const ComConfig& comConfig);
};
}

#endif //BUILD_SRC_ROBOCOON_CONTROL_HARDWARE_SRC_COM_H_
