#ifndef ROBOCOON_CONTROL_HARDWARE_WHEEL_CONTROLLER_INTERFACE_H
#define ROBOCOON_CONTROL_HARDWARE_WHEEL_CONTROLLER_INTERFACE_H

#include "hardware_interface/handle.hpp"
#include "hardware_interface/hardware_info.hpp"
#include "hardware_interface/system_interface.hpp"
#include "hardware_interface/types/hardware_interface_return_values.hpp"
#include "rclcpp/macros.hpp"
#include "rclcpp/logging.hpp"
#include "rclcpp/clock.hpp"
#include "wheel_speeds.h"
#include "com.h"
#include <optional>
#include <deque>

namespace robocoon_control_hardware {
class WheelControllerInterface : public hardware_interface::SystemInterface {
  static constexpr size_t NUM_OF_WHEELS = 4;
 public:

  RCLCPP_SHARED_PTR_DEFINITIONS(WheelControllerInterface)

  hardware_interface::return_type configure(const hardware_interface::HardwareInfo &system_info) override;

  std::vector<hardware_interface::StateInterface> export_state_interfaces() override;

  std::vector<hardware_interface::CommandInterface> export_command_interfaces() override;

  hardware_interface::return_type start() override;

  hardware_interface::return_type stop() override;

  std::string get_name() const override;

  hardware_interface::status get_status() const override;

  hardware_interface::return_type read() override;

  hardware_interface::return_type write() override;

 private:
  void processWheelSpeeds(std::array<double, NUM_OF_WHEELS> speeds);

  std::array<double,NUM_OF_WHEELS> m_commands = {0};
  std::array<double,NUM_OF_WHEELS> m_state = {0};
  std::array<double,NUM_OF_WHEELS> m_multipliers = {1.};

  std::array<std::deque<double>, NUM_OF_WHEELS> m_speeds;

  std::string m_name;
  double m_zero = 0.;

  std::unique_ptr<Com> m_serialPort;

  rclcpp::Clock m_clock;

  hardware_interface::status m_status = hardware_interface::status::UNKNOWN;
  std::vector<hardware_interface::StateInterface> m_stateInterfaces;
  std::vector<hardware_interface::CommandInterface> m_commandInterfaces;

  rclcpp::Logger m_logger = rclcpp::get_logger("WheelControllerInterface");
};
} // namespace robocoon_control_hardware

#endif //ROBOCOON_CONTROL_HARDWARE_WHEEL_CONTROLLER_INTERFACE_H
