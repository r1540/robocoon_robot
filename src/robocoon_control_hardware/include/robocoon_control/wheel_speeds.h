#ifndef ROBOCOON_CONTROL_HARDWARE_WHEEL_SPEEDS_H_
#define ROBOCOON_CONTROL_HARDWARE_WHEEL_SPEEDS_H_

namespace robocoon_control_hardware {
struct WheelSpeeds {
  double frontRight, frontLeft, rearRight, rearLeft;

  static WheelSpeeds zero() {
    return {0., 0., 0., 0.};
  }
};
}

#endif //ROBOCOON_CONTROL_HARDWARE_WHEEL_SPEEDS_H_
