from robocoon_wireless_msgs.srv import SignalInfo

import rclpy
from rclpy.node import Node


class WirelessTelemetry(Node):

    def __init__(self):
        super().__init__('wireless_telemetry')
        self.srv = self.create_service(SignalInfo, 'signal_info', self.get_signal_info)
        self.get_logger().info("wireless_telemetry node started")

    def get_signal_info(self, _, response):
        with open("/proc/net/wireless") as info:
            tokens = (info.readlines()[-1]).split()
        response.strength = float(tokens[3])
        response.noise = float(tokens[4])
        return response


def main(args=None):
    rclpy.init(args=args)
    wireless_telemetry = WirelessTelemetry()
    rclpy.spin(wireless_telemetry)
    rclpy.shutdown()


if __name__ == '__main__':
    main()
